//
// Created by jonathan.hanks on 12/20/19.
//

#ifndef DAQD_TRUNK_SIMPLE_EPICS_HH
#define DAQD_TRUNK_SIMPLE_EPICS_HH

#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <type_traits>
#include <gddAppFuncTable.h>
#include <epicsTimer.h>

#include "casdef.h"
#include "gddApps.h"
#include "smartGDDPointer.h"

#include "simple_pv.h"

namespace simple_epics
{
    namespace detail
    {
        class simplePVBase : public casPV
        {
        public:
            simplePVBase( ) : casPV( )
            {
            }
            ~simplePVBase( ) override = default;

            virtual void update( ) = 0;
        };
    } // namespace detail

    class Server;

    enum class PVMode
    {
        ReadOnly = 0,
        ReadWrite = 1,
    };

    /*!
     * @brief A description of a PV, used to describe an int PV to the server.
     * @note this is given a pointer to the data.  This value is only read
     * when a Server object is told to update its data.
     */
    template < typename NumType >
    class pvBasicNumericAttributes
    {
        static_assert( std::is_integral< NumType >::value ||
                           std::is_floating_point< NumType >::value,
                       "integer or floating point type required" );

    public:
        using value_type = NumType;

        pvBasicNumericAttributes( std::string                   pv_name,
                                  std::atomic< NumType >*       value,
                                  std::pair< NumType, NumType > alarm_range,
                                  std::pair< NumType, NumType > warn_range,
                                  PVMode mode = PVMode::ReadOnly )
            : name_{ std::move( pv_name ) },

              alarm_low_{ alarm_range.first },
              alarm_high_{ alarm_range.second }, warn_low_{ warn_range.first },
              warn_high_{ warn_range.second }, mode_{ mode }, src_{ value }
        {
        }

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

        NumType
        alarm_high( ) const noexcept
        {
            return alarm_high_;
        }
        NumType
        alarm_low( ) const noexcept
        {
            return alarm_low_;
        }
        NumType
        warn_high( ) const noexcept
        {
            return warn_high_;
        }
        NumType
        warn_low( ) const noexcept
        {
            return warn_low_;
        }

        const std::atomic< NumType >*
        src( ) const noexcept
        {
            return src_;
        }

        PVMode
        mode( ) const noexcept
        {
            return mode_;
        }

    private:
        std::string name_;

        NumType alarm_high_;
        NumType alarm_low_;
        NumType warn_high_;
        NumType warn_low_;

        PVMode                  mode_;
        std::atomic< NumType >* src_{ nullptr };
    };
    using pvIntAttributes = pvBasicNumericAttributes< std::int32_t >;
    using pvUIntAttributes = pvBasicNumericAttributes< std::uint32_t >;
    using pvUShortAttributes = pvBasicNumericAttributes< std::uint16_t >;
    using pvFloatAttributes = pvBasicNumericAttributes< float >;
    using pvDoubleAttributes = pvBasicNumericAttributes< double >;
    static_assert( sizeof( std::int32_t ) == sizeof( int ),
                   "int must be 32 bit" );

    /*!
     * @brief A description of a PV, used to describe a string PV to the server.
     * @note this is given a pointer to the data.  This value is only read
     * when a Server object is told to update its data.
     */
    class pvStringAttributes
    {
    public:
        pvStringAttributes( std::string  pv_name,
                            ::StringPVIO io,
                            PVMode       mode = PVMode::ReadOnly )
            : name_{ std::move( pv_name ) }, mode_{ mode }, io_{ io }
        {
        }

        pvStringAttributes( std::string pv_name, char* data );

        const std::string&
        name( ) const noexcept
        {
            return name_;
        }

        PVMode
        mode( ) const noexcept
        {
            return mode_;
        }

        const ::StringPVIO&
        io( ) const noexcept
        {
            return io_;
        }

    private:
        std::string  name_;
        PVMode       mode_;
        ::StringPVIO io_;
    };

    /*!
     * @brief An R/O implementation of the Portable CA Server.
     */
    class Server : public caServer
    {
    public:
        explicit Server( std::string prefix = "" )
            : caServer( ), pvs_{ }, prefix_{ std::move( prefix ) }
        {
        }
        ~Server( ) override;

        /*!
         * @brief Add a PV to the server.
         */
        void addPV( pvUShortAttributes attr );
        void addPV( pvIntAttributes attr );
        void addPV( pvUIntAttributes attr );
        void addPV( pvStringAttributes attr );
        void addPV( pvFloatAttributes attr );
        void addPV( pvDoubleAttributes attr );

        const std::string&
        prefix( ) noexcept
        {
            return prefix_;
        }

        /*!
         * @brief Reflect all changes in the data for each PV into the server
         */
        void update( );

        pvExistReturn pvExistTest( const casCtx&    ctx,
                                   const caNetAddr& clientAddress,
                                   const char*      pPVAliasName ) override;

        pvAttachReturn pvAttach( const casCtx& ctx,
                                 const char*   pPVAliasName ) override;

    private:
        std::mutex                                                       m_;
        std::map< std::string, std::unique_ptr< detail::simplePVBase > > pvs_;
        std::string prefix_;
    };

} // namespace simple_epics

#endif // DAQD_TRUNK_SIMPLE_EPICS_HH
