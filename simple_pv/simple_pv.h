#ifndef __STREAMING_SIMPLE_PV_H__
#define __STREAMING_SIMPLE_PV_H__

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SIMPLE_PV_INT 0
#define SIMPLE_PV_STRING 1
#define SIMPLE_PV_DOUBLE 2

#define SIMPLE_PV_READ_ONLY 0
#define SIMPLE_PV_READ_WRITE 1

/*!
 * @brief A Simple structure representing a read-only PV
 * This is used when constructing a large number of PVs
 * specified in an array.  It contains fields that do
 * not always make sense for the data type.
 */
typedef struct SimplePV
{
    const char* name;
    int         pv_type; /// SIMPLE_PV_INT or SIMPLE_PV_STRING
    void*       data;

    // These values are only used for an int or double pv
    // they will be cased to the appropriate type
    double alarm_high;
    double alarm_low;
    double warn_high;
    double warn_low;
} SimplePV;

/* Individual structures for adding via simple_pv_server_add_* */

typedef struct SimpleUInt16PV
{
    const char* name;
    // this should be an uint16 which is read/written atomically
    void*    data;
    int      read_write;
    uint16_t alarm_high;
    uint16_t alarm_low;
    uint16_t warn_high;
    uint16_t warn_low;
} SimpleUInt16PV;

typedef struct SimpleInt32PV
{
    const char* name;
    // this should be an int32 which is read/written atomically
    void*   data;
    int     read_write;
    int32_t alarm_high;
    int32_t alarm_low;
    int32_t warn_high;
    int32_t warn_low;
} SimpleInt32PV;

typedef struct SimpleFloat64PV
{
    const char* name;
    // this should be a double which is read/written atomically
    void*  data;
    int    read_write;
    double alarm_high;
    double alarm_low;
    double warn_high;
    double warn_low;
} SimpleFloat64PV;

typedef struct SimpleFloat32PV
{
    const char* name;
    // this should be a float which is read/written atomically
    void* data;
    int   read_write;
    float alarm_high;
    float alarm_low;
    float warn_high;
    float warn_low;
} SimpleFloat32PV;

typedef struct StringValue
{
    const char* str;
} StringValue;

extern StringValue string_value_create( const char* input );
extern void        string_value_destroy( StringValue* sv );

/**
 * @brief StringPVIO provides generic IO to a string value
 * @Note this is provided as there is no atomic string type.
 * This allows an application to wrap access to the string in
 * a mutex or some other lock as needed.
 *
 * As an example a StringPVIO structure is created for any string
 * values used in the call to simple_pv_server_create, however the
 * callbacks do no locking and deny writes to the PV from EPICS.
 */
typedef struct StringPVIO
{
    // data value to be passed to read_fn and write_fn
    void* user_handle;

    // read_fn, extract data from the string backing store
    // it is the callers responsibility to clean up the returned value
    // it is a copy.
    // call as
    // StringValue sv = read_fn(user_handle);
    // ...
    // string_value_destroy(&sv);
    StringValue ( *read_fn )( void* );

    // write_fn, write data to the string backing store
    // call as write_fn(user_handle, string_value);
    int ( *write_fn )( void*, const char* );
} StringPVIO;

typedef struct SimpleStringPV
{
    const char* name;
    StringPVIO  io;
    int         read_write;
} SimpleStringPV;

typedef void* simple_pv_handle;

/*!
 * @brief Create a pCas server to export the given list of PVS
 * @param prefix Prefix to add to the name of each PV
 * @param pvs A array of SimplePV structures describing the data to export into
 * EPICS.  May be empty.
 * @param pv_count The number of entries in pvs
 *
 * @returns NULL on error, else a handle object representing the pCas server
 *
 * @note call simple_pv_server_destroy to destroy the returned server
 * @note PVS constructed during this call are read only and are assumed
 * to be accessed in a singly threaded manner.  If you need more complex
 * situations and multi threads the various _add* functions will be a better
 * fit, especially in handling strings.
 */
simple_pv_handle
simple_pv_server_create( const char* prefix, SimplePV* pvs, int pv_count );

/*
 * The following functions allow creating a PV with more capabilities than
 * the default set used in simple_pv_server_create.
 *
 * Each call takes a Simple*PV structure which allows for Read/Write to be
 * specified.
 */

/*!
 * @brief Add a 32bit integer PV to the server
 * @param server the pCas server
 * @param pv The input PV information
 * @return 1 on success, else 0
 */
int simple_pv_server_add_int32( simple_pv_handle server, SimpleInt32PV pv );

/*!
 * @brief Add a 16bit unsigned integer PV to the server
 * @param server the pCas server
 * @param pv The input PV information
 * @return 1 on success, else 0
 */
int simple_pv_server_add_uint16( simple_pv_handle server, SimpleUInt16PV pv );

/*!
 * @brief Add a 32bit float PV to the server
 * @param server the pCas server
 * @param pv The input PV information
 * @return 1 on success, else 0
 */
int simple_pv_server_add_float32( simple_pv_handle server, SimpleFloat32PV pv );

/*!
 * @brief Add a 64bit float PV to the server
 * @param server the pCas server
 * @param pv The input PV information
 * @return 1 on success, else 0
 */
int simple_pv_server_add_float64( simple_pv_handle server, SimpleFloat64PV pv );

/*!
 * @brief Add a string PV to the server
 * @param server the pCas server
 * @param pv The input PV information
 * @return 1 on success, else 0
 */
int simple_pv_server_add_string( simple_pv_handle server, SimpleStringPV pv );

/*!
 * @brief Given a handle to a pCas server, update the data being reflected into
 * epics. This causes the data field re-read and reflected into EPICS.
 * @param server The pCas server
 *
 * @note It is safe to call this if server == NULL
 */
void simple_pv_server_update( simple_pv_handle server );

/*!
 * @brief Destroy and close down a pCas server created by
 * simple_pv_server_create.
 * @param server
 *
 * @note It is safe to call this if server == NULL or *server == NULL
 */
void simple_pv_server_destroy( simple_pv_handle* server );

#ifdef __cplusplus
}
#endif

#endif /* __STREAMING_SIMPLE_PV_H__ */