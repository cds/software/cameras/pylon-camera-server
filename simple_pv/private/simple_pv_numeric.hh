//
// Created by jonathan.hanks on 3/16/22.
//

#ifndef DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH
#define DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH

#include "simple_pv_types.hh"
#include "simple_pv_internal.hh"
#include <mutex>
#include <type_traits>

#include <iostream>

namespace simple_epics
{

    namespace detail
    {

        /*!
         * @brief A representation of a R/O integer in a PV
         */
        template < typename NumType >
        class simpleBasicNumericPV : public simplePVBase
        {
            static_assert( std::is_integral< NumType >::value ||
                               std::is_floating_point< NumType >::value,
                           "Must use an integer or floating point type" );

        public:
            simpleBasicNumericPV( caServer&                           server,
                                  pvBasicNumericAttributes< NumType > attr )
                : simplePVBase( ), server_{ server }, attr_{ std::move(
                                                          attr ) },
                  val_( ), monitored_{ false }
            {
                static std::once_flag initted{ };
                std::call_once( initted, []( ) {
                    simpleBasicNumericPV::setup_func_table( );
                } );

                val_ = new gddScalar( gddAppType_value,
                                      ait_data_type< NumType >::value );
                val_->unreference( );
                set_value( *attr_.src( ) );
            }
            ~simpleBasicNumericPV( ) override = default;

            caStatus
            read( const casCtx& ctx, gdd& prototype ) override
            {
                return get_func_table( ).read( *this, prototype );
            }
            caStatus
            write( const casCtx& ctx, const gdd& value ) override
            {
                if ( attr_.mode( ) != PVMode::ReadWrite )
                {
                    return S_casApp_noSupport;
                }
                aitType newValue;
                value.get( &newValue, ait_data_type< NumType >::value );
                set_value( newValue );
                const_cast< std::atomic< NumType >* >( attr_.src( ) )
                    ->store( newValue );
                return S_casApp_success;
            }

            void destroy( ) override{ };

            aitEnum
            bestExternalType( ) const override
            {
                return val_->primitiveType( );
            }

            const char*
            getName( ) const override
            {
                return attr_.name( ).c_str( );
            }

            caStatus
            interestRegister( ) override
            {
                monitored_ = true;
                return S_casApp_success;
            }

            void
            interestDelete( ) override
            {
                monitored_ = false;
            }

            void
            update( ) override
            {
                set_value( *attr_.src( ) );
            }

        private:
            using aitType = typename ait_data_type< NumType >::ait_type;

            void
            set_value( aitType value )
            {
                aitType current_value = 0;

                val_->getConvert( current_value );
                if ( current_value == value )
                {
                    return;
                }

                val_->putConvert( value );
                aitTimeStamp ts = aitTimeStamp( epicsTime::getCurrent( ) );
                val_->setTimeStamp( &ts );

                aitUint16 stat = epicsAlarmNone;
                aitUint16 sevr = epicsSevNone;
                if ( value >= attr_.alarm_high( ) )
                {
                    stat = epicsAlarmHiHi;
                    sevr = epicsSevMajor;
                }
                else if ( value <= attr_.alarm_low( ) )
                {
                    stat = epicsAlarmLoLo;
                    sevr = epicsSevMajor;
                }
                else if ( value >= attr_.warn_high( ) )
                {
                    stat = epicsAlarmHigh;
                    sevr = epicsSevMinor;
                }
                else if ( value <= attr_.warn_low( ) )
                {
                    stat = epicsAlarmLow;
                    sevr = epicsSevMinor;
                }
                val_->setSevr( sevr );
                val_->setStat( stat );

                if ( monitored_ )
                {
                    casEventMask mask =
                        casEventMask( server_.valueEventMask( ) );
                    bool alarm_changed = ( stat != val_->getStat( ) ||
                                           sevr != val_->getSevr( ) );
                    if ( alarm_changed )
                    {
                        mask |= server_.alarmEventMask( );
                    }
                    postEvent( mask, *val_ );
                }
            }

            static void
            setup_func_table( )
            {
                auto install = []( const char* name,
                                   gddAppFuncTableStatus (
                                       simpleBasicNumericPV::*handler )(
                                       gdd& ) ) {
                    gddAppFuncTableStatus status;

                    //           char error_string[100];

                    status = get_func_table( ).installReadFunc( name, handler );
                    if ( status != S_gddAppFuncTable_Success )
                    {
                        //                errSymLookup(status, error_string,
                        //                sizeof(error_string));
                        //               throw std::runtime_error(error_string);
                        throw std::runtime_error(
                            "Unable to initialize pv lookup table" );
                    }
                };

                install( "units",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "status", &simpleBasicNumericPV::read_status );
                install( "severity", &simpleBasicNumericPV::read_severity );
                install( "maxElements",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "precision", &simpleBasicNumericPV::read_precision );
                install( "alarmHigh", &simpleBasicNumericPV::read_alarm_high );
                install( "alarmLow", &simpleBasicNumericPV::read_alarm_low );
                install( "alarmHighWarning",
                         &simpleBasicNumericPV::read_warn_high );
                install( "alarmLowWarning",
                         &simpleBasicNumericPV::read_warn_low );
                install( "maxElements",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "graphicHigh",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "graphicLow",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "controlHigh",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "controlLow",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "enums",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "menuitem",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "timestamp",
                         &simpleBasicNumericPV::read_attr_not_handled );
                install( "value", &simpleBasicNumericPV::read_value );
            }
            static gddAppFuncTable< simpleBasicNumericPV >&
            get_func_table( )
            {
                static gddAppFuncTable< simpleBasicNumericPV > func_table;
                return func_table;
            }

            gddAppFuncTableStatus
            read_attr_not_handled( gdd& g )
            {
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_status( gdd& g )
            {
                g.putConvert( val_->getStat( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_severity( gdd& g )
            {
                g.putConvert( val_->getSevr( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_precision( gdd& g )
            {
                g.putConvert( 0 );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_alarm_high( gdd& g )
            {
                g.putConvert( attr_.alarm_high( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_alarm_low( gdd& g )
            {
                g.putConvert( attr_.alarm_low( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_warn_high( gdd& g )
            {
                g.putConvert( attr_.warn_high( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_warn_low( gdd& g )
            {
                g.putConvert( attr_.warn_low( ) );
                return S_casApp_success;
            }

            gddAppFuncTableStatus
            read_value( gdd& g )
            {
                auto status = gddApplicationTypeTable::app_table.smartCopy(
                    &g, val_.get( ) );
                return ( status ? S_cas_noConvert : S_casApp_success );
            }

            caServer&                           server_;
            pvBasicNumericAttributes< NumType > attr_;
            smartGDDPointer                     val_;
            bool                                monitored_;
        };
        using simpleIntPV = simpleBasicNumericPV< std::int32_t >;
        using simpleUIntPV = simpleBasicNumericPV< std::uint32_t >;
        using simpleUShortPV = simpleBasicNumericPV< std::uint16_t >;
        using simpleFloatPV = simpleBasicNumericPV< float >;
        using simpleDoublePV = simpleBasicNumericPV< double >;
    } // namespace detail
} // namespace simple_epics

#endif // DAQD_TRUNK_SIMPLE_EPICS_INTERNAL_INT_HH
