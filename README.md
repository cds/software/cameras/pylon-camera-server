# pylon-camera-server

**H264 RTP video streaming server and EPICS IOC for Basler 2D cameras**

Uses the Basler Pylon SDK for acquiring frames from Basler 2D cameras. Encodes the video as H264 and payloads it into RTP packets for streaming. Provides EPICS channels for monitoring image calculations and controlling camera settings. Can be configured to periodically export frames to TIF image files. Supports all Basler 2D cameras that are supported by the Basler Pylon SDK.

## Requirements

Debian packages:

### Build

- [pylon](https://www.baslerweb.com/en/products/basler-pylon-camera-software-suite/) 7.1.0.25066-deb0
- libboost-dev
- libopencv-core-dev
- libopencv-imgproc-dev
- libopencv-dev
- libgstreamer1.0-dev
- libgstreamer-plugins-base1.0-dev
- cmake
- epics-dev

### Run

- [pylon](https://www.baslerweb.com/en/products/basler-pylon-camera-software-suite/) 7.1.0.25066-deb0
- gstreamer1.0-plugins-base
- gstreamer1.0-plugins-good
- gstreamer1.0-plugins-ugly
- gstreamer1.0-x

## Usage

```
pylon-camera-server FILE
FILE: The path to an ini configuration file
```

### Configuration file

Example:
```
serial_number=40042918
packet_size=8192
inter_packet_delay=50000
exposure_auto=0
exposure_time_raw=3000
gain_auto=0
gain_raw=0
timeout_ms=5000
width=720
height=540
framerate_numerator=25
framerate_denominator=1
format=GRAY8
overlay_text=SQZT CAM1
overlay_silent=false
multicast_address=239.0.0.0
multicast_port=5000
ttl_mc=64
channel_prefix=H1:VID-CAM1_
calc_noise_floor=25
archive_interval_minutes=0
archive_folder_path_prefix=/ligo/data/camera/archive/
archive_file_name_prefix=h1cam01
```

- **serial_number**: The serial number of the camera to connect to.
- **packet_size**: The packet size in bytes. Excludes data leader and data trailer. [default 8192]
- **inter_packet_delay**: The delay between the transmission of each packet. The delay is measured in ticks. [default 50000]
- **exposure_auto**: *{0 = Off, 1 = Once, 2 = Continuous}* The camera's auto exposure setting. Also controlled by the `AUTO` EPICS channel. [default 0]
- **exposure_time_raw**: The camera's exposure time. Also controlled by the `EXP` EPICS channel. [default 3000]
- **gain_auto**: *{0 = Off, 1 = Once, 2 = Continuous}* The camera's auto gain setting. Also controlled by the `GAIN_AUTO` EPICS channel. [default 0]
- **gain_raw**: The camera's gain. Also controlled by the `GAIN` EPICS channel. [default 0]
- **timeout_ms**: The timeout in ms for grabbing images from the camera. Also controlled by the `TIMEOUT` EPICS channel. [default 5000]
- **width**: The width of the camera's sensor in pixels.
- **height**: The height of the camera's sensor in pixels.
- **framerate_numerator**: The numerator of the fractional number used for the camera's frame rate. [default 25]
- **framerate_denominator**: The denominator of the fractional number used for the camera's frame rate. [default 1]
- **format**: The image format. See gst-plugin-pylon for supported [format names](https://github.com/basler/gst-plugin-pylon/#pixel-format-definitions). [default GRAY8]
- **overlay_text**: The overlay text.
- **overlay_silent**: *{True, False}* If the overlay text and clock should be hidden from the video stream. [default false]
- **multicast_address**: The virtual IP address of the multicast group to stream the video to. This should be in the range 239.0.0.0 - 239.255.255.255. https://www.iana.org/assignments/multicast-addresses/multicast-addresses.xhtml
- **multicast_port**: The multicast port to stream the video to. [default 5000]
- **ttl_mc**: The multicast TTL parameter. [default 64]
- **channel_prefix**: The prefix for each EPICS channel name.
- **calc_noise_floor**: The pixel value cutoff limit to use before image statistics calculations. [default 25]
- **archive_interval_minutes**: The period in minutes for the repeated archival of frames as TIF images. Set to 0 to disable. Also controlled by the `ARCHIVE_INTERVAL` EPICS channel. [default 0]
- **archive_folder_path_prefix**: The prefix of the path to the folder to archive periodic snapshots of the video stream in.
- **archive_file_name_prefix**: The prefix for the archive image file names.


### EPICS channels
- {channel_prefix}**X**: The horizontal position of the image centroid. *(read only)*
- {channel_prefix}**Y**: The vertical position of the image centroid. *(read only)*
- {channel_prefix}**WX**: The Gaussian width in the X direction. *(read only)*
- {channel_prefix}**WY**: The Gaussian width in the Y direction. *(read only)*
- {channel_prefix}**SUM**: The image pixel sum. *(read only)*
- {channel_prefix}**VALID**: 1 if and only if there has been a successful iteration through the image grab loop and the last iteration through the image grab loop succeeded. *(read only)*
- {channel_prefix}**SATURATED**: 1 if and only if the maximum pixel value of the frame is greater than or equal to the pixel dynamic range. *(read only)*
- {channel_prefix}**EXP_AUTO_REQ**: *{ 0 = Off, 1 = Once, 2 = Continuous }* The user requested auto exposure setting. *(read & write)*
- {channel_prefix}**EXP_AUTO_SET**: Set greater than 0 to trigger the setting of the camera auto exposure setting from the user requested auto exposure setting. *(read & write)*
- {channel_prefix}**EXP_AUTO**: *{ 0 = Off, 1 = Once, 2 = Continuous }* The camera auto exposure setting. *(read only)*

- {channel_prefix}**EXP_REQ**: The user requested exposure time. *(read & write)*
- {channel_prefix}**EXP_SET**: Set greater than 0 to trigger the setting of the camera exposure time from the user requested exposure time. *(read & write)*
- {channel_prefix}**EXP**: The camera exposure time. *(read only)*

- {channel_prefix}**GAIN_AUTO_REQ**: *{ 0 = Off, 1 = Once, 2 = Continuous }* The user requested auto gain setting. *(read & write)*
- {channel_prefix}**GAIN_AUTO_SET**: Set greater than 0 to trigger the setting of the camera auto gain setting from the user requested auto gain setting. *(read & write)*
- {channel_prefix}**GAIN_AUTO**: *{ 0 = Off, 1 = Once, 2 = Continuous }* The camera auto gain setting. *(read only)*

- {channel_prefix}**GAIN_REQ**: The user requested gain. *(read & write)*
- {channel_prefix}**GAIN_SET**: Set greater than 0 to trigger the setting of the camera gain from the user requested gain. *(read & write)*
- {channel_prefix}**GAIN**: The camera gain. *(read only)*

- {channel_prefix}**TIMEOUT**: The timeout in ms for grabbing images from the camera. *(read & write)*

- {channel_prefix}**ARCHIVE_INTERVAL**: The period in minutes for the repeated archival of frames as TIF images. Set to 0 to disable. *(read & write)*

## Setup

### Debian 11

- Install Pylon.
- Create a file named `pylon.conf` in `/etc/ld.so.conf.d/` that contains the text `/opt/pylon/lib` and then run `ldconfig`.

Example building and installing a Debian package from source (change `<version>` in the last step to match):
```
git clone git@git.ligo.org:cds/software/cameras/pylon-camera-server.git
cd pylon-camera-server
git checkout debian/bullseye
dpkg-buildpackage -us -uc --build=binary
cd ..
sudo apt install ./pylon-camera-server_<version>.deb
```

- Add a `<site name>.conf` file in `/etc/systemd/system/pylon-camera-server@.service.d/` containing `[Service]\nWorkingDirectory=<custom path>` changing `<custom path>` to the full path of the folder to store the configuration files in. Example: `/etc/systemd/system/pylon-camera-server@.service.d/lho.conf` containing `[Service]\nWorkingDirectory=/opt/rtcds/ifo/lho-pylon-camera-config`.
- Create configuration files for each camera in the folder specified in the previous step. The file names must end in `.ini`. Example: `H1-VID-CAM-FC2.ini`.
- Start a `systemd` service for each camera. Example: `service pylon-camera-server@H1-VID-CAM-FC2 start`

## Architecture

![pipeline](pipeline.svg)


The pylonsrc element is a GStreamer appsrc element that uses the Pylon SDK to acquire frames from the camera. It also controls the camera settings. The camera to connect to is selected by the `serial_number` configuration file setting. It can be any Basler 2D camera on the network that is supported by the Pylon library.

The capsfilter element sets the frame rate and image width, height, and format from the `framerate_numerator`, `framerate_denominator`, `width`, `height`, and `format` configuration file settings.

The tee element copies the video stream to a pair of parallel pipelines.

The first pipeline sends its copy of the video stream to an appsink element. The appsink element is used to calculate statistics for each frame. If the `archive_interval_minutes` configuration file setting is greater than 0, then it also saves a frame as a TIF image every `archive_interval_minutes`. The TIF images are archived at the concatenation of the path supplied by the `archive_folder_path_prefix` configuration file setting and the date. Their file names are the concatenation of the name supplied by the `archive_file_name_prefix` configuration file setting and the date and time.

The videoconvert element converts each frame to a format supported by the x264enc element.

If the `overlay_silent` configuration file setting is set to false then the clockoverlay element in the second pipeline overlays the name supplied by the `overlay_text` configuration file setting and the current date and time onto the top left corner of each frame.

The x264enc element encodes the video into H264 compressed data that is then payload-encoded into RTP packets by the rtph264pay element and streamed by the udpsink element to the multicast address and port specified by the `host` and `port` configuration file settings.
